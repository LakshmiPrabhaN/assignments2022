import React, { useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
 
const Login = () => {
 const [name, setName] = useState("");
 const [password, setPassword] = useState("");
 const [value, setValue] = useState("");
 const updateValue = (event) => {
   let value=event.target.value;
     console.log(password)
     if (password === "password"){
       value = "Hello Mr/s "+name;
     }
     else{
       value ="Invalid Password";        
     }
     setValue(value);
 };
 return (
   <div>
      <form onSubmit={updateValue}>
       <Form.Label>Email</Form.Label>
       <input type="email"/><br></br>
       <Form.Label>Name</Form.Label>
       <input type="text" value={name} onChange={(e) => setName(e.target.value)}/><br></br>
       <Form.Label>Password</Form.Label>
       <input type="password" value={password} onChange={(e) => setPassword(e.target.value)}/><br></br>
       <Button type="submit">Login</Button>
      </form>
     <h1>{value}</h1>
   </div>
 );
};
export default Login;
