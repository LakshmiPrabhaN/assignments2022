import './App.css';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from './Home'
import Aboutus from './Aboutus'
import Services from './Services'
 
function App() {
 return (
   <BrowserRouter>
   <Routes>
   <Route path="/home" element= {<Home />}>    </Route>
   <Route path="/about" element= {<Aboutus />}>    </Route>
   <Route path="/service" element= {<Services />}>    </Route>
   </Routes>
  </BrowserRouter>
 );
}
 
export default App;
